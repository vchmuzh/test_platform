var express = require('express');
var http = require('http');
var path = require('path');
var WebSocketServer = require('ws').Server;

var app = express();

app.use(express.static(path.join(__dirname, 'public')));

var server = http.createServer(app);
server.listen(8080, function(){
    console.log('Express server listening on port ' + 8080);
});

var webSocketServer = new WebSocketServer({server: server});
webSocketServer.on('connection', function(ws) {
    var count = 0;
    var timer = setInterval(function() {
        count++;
        ws.send(JSON.stringify({count: count}), function(err) {
            if(err) console.error(err);
        })
    },1000);

    console.log('client connct');

    ws.on('close', function() {
        console.log('client DISCONNECT');
        clearInterval(timer);
    })

});

