/*
 * USAGE:
 *
 *  <link rel="stylesheet" href="/css/nodechat.css">
    <script src="/vendor/bower_components/socket.io-client/dist/socket.io.js"></script>
    <script src="/js/nodechat-client.js"></script>



 <div class="column" id="chat_cnt"></div>

 <script>
     new NodeChat({
         targetContainer: $('#chat_cnt'),
         viewportHeight: 400,
         url: 'ws://localhost:8080',
         room:'room1'
     });
 </script>

 *
 *
 * */



function NodeChat(options) {
    var self = this;

    this.options = {
        viewportHeight: 400,
        url: '',
        room:'room'
    };
    $.extend(this.options, options);

    this.options.targetContainer.html(this.chatTemplate);

    this.ui = {};
    this.ui.chatContainer = options.targetContainer.find('#chat_container');
    this.ui.chatViewport = options.targetContainer.find('#chat_vieport');
    this.ui.usersContainer = options.targetContainer.find('#chat_users');
    this.ui.usersList = options.targetContainer.find('#chat_users_list');
    this.ui.usersToggleButton = options.targetContainer.find('#chat_users_toggle');
    this.ui.messages = options.targetContainer.find('#chat_messages');
    this.ui.form = options.targetContainer.find('#chat_form');
    this.ui.input = options.targetContainer.find('#chat_input');
    this.ui.clearBtn = options.targetContainer.find('#chat_clear_input');
    this.ui.sendBtn = options.targetContainer.find('#chat_send');

    this.setHeight(this.options.viewportHeight);
    this.createConnection();
    this.bindSocketEventHandlers();
    this.bindUiEvents();
}

NodeChat.prototype.setHeight = function(height) {
    this.ui.chatContainer.height(height);
};

NodeChat.prototype.bindUiEvents = function() {
    var self = this;

    this.ui.usersList.on('click', '.js-chat-user', function(e){
        self.setMessageTo($(this).text());
        return false;
    });
    this.ui.clearBtn.on('click', function(){self.clearInput()});
    this.ui.usersToggleButton.on('click', function() {
        if(self.ui.usersContainer.width() == 0) {
            self.ui.usersContainer.animate({width:200}, 500);
            self.ui.chatViewport.animate({'margin-right':200}, 500);
        } else {
            self.ui.usersContainer.animate({width:0}, 500);
            self.ui.chatViewport.animate({'margin-right':0}, 500);
        }
    });
    this.ui.form.on('submit', function(e){return false});
};

NodeChat.prototype.createConnection = function() {
    this.socket = io.connect(this.options.url, {
        reconnect: false
    });
};

NodeChat.prototype.bindSocketEventHandlers = function() {
    var self = this;
    this.socket
        .on('connect', function () {
            self.socket.emit('room', self.options.room, function(err) {
                if(err) console.error(err);
            });

            self.printStatus('соединение установлено', 'success');
            self.ui.input.on('keydown', onKeyDown);
            self.ui.sendBtn.on('click', sendMessage);
            self.ui.input.prop('disabled', false);
            self.ui.sendBtn.prop('disabled', false);
        })
        .on('message', function(name, text, date){ self.printMessage(name, text, date) })
        .on('join', function(name, id){
            self.joinUser(name, id);
        })
        .on('self_join', function(activeUsers){
            self.joinSelf(activeUsers);
        })
        .on('leave', function(name, id){
            self.leaveUser(name, id);
        })
        .on('logout', function () {
            if(self.options.onLogout) self.options.onLogout();
        })
        .on('disconnect', function () {
            self.ui.usersList.html('');
            self.printStatus('соединение потеряно', 'danger');
            self.ui.input.off('keydown', onKeyDown);
            self.ui.sendBtn.off('click', sendMessage);
            self.ui.input.prop('disabled', true);
            self.ui.sendBtn.prop('disabled', true);
            setTimeout(reconnect, 500);
        });

    function onKeyDown(e) {
        if(e.keyCode == 13) {
            self.sendMessage();
        }
    }

    function reconnect() {
        self.socket.once('error', function() {
            setTimeout(reconnect, 500);
        });
        self.socket.socket.connect();
    }

    function sendMessage() {
        self.sendMessage();
    }
};

NodeChat.prototype.sendMessage = function() {
    var self = this;
    var text = escapeHtml( this.ui.input.val() );
    this.socket.emit('message', text, function(name, date) {
        self.printMessage(name, text, date);
    });
    this.ui.input.val('');

    function escapeHtml(str) {
        var div = document.createElement('div');
        div.appendChild(document.createTextNode(str));
        return div.innerHTML;
    }
    return false;
};

NodeChat.prototype.printMessage = function(name, text, date) {
    $('<li class="chat_item chat_message"><b class="chat_message_name">' + name + '</b> <i class="chat_message_date">('+ date +')</i>: <span class="chat_message_text">' + text + '</span></li>').appendTo(this.ui.messages);
    this.scrollToBootom();
};

NodeChat.prototype.printEvent = function(text) {
    $('<li class="chat_item chat_event">'+ text +'</li>').appendTo(this.ui.messages);
    this.scrollToBootom();
};

NodeChat.prototype.printStatus = function(text, status) {
    $('<li class="alert alert-'+ status +' chat_item chat_status">'+ text +'</li>').appendTo(this.ui.messages);
    this.scrollToBootom();
};

NodeChat.prototype.joinUser = function(name, id) {
    this.ui.usersList.append('<a href="#" id="chat_user_' + id + '" class="js-chat-user list-group-item">'+ name +'</a>');
    this.printEvent(' :) В чат вошел(а) <b>' + name +'</b>');
};

NodeChat.prototype.joinSelf = function(activeUsers) {
    var items = [];
    $.each(activeUsers, function(i, item) {
        items.push('<a href="#"  id="chat_user_' + item.id + '" class="js-chat-user list-group-item">'+ item.name +'</a>');
    });
    this.ui.usersList.append( items.join('') );
};

NodeChat.prototype.leaveUser = function(name, id) {
    //debugger
    this.ui.usersList.find('#chat_user_' + id).remove();
    this.printEvent(':( Чат покинул(а) <b>' + name +'</b>');
};

NodeChat.prototype.setMessageTo = function(username) {
    this.ui.input.val(username + ', ' + this.ui.input.val());
    this.ui.input.focus();
};

NodeChat.prototype.clearInput = function() {
    this.ui.input.val('');
};

NodeChat.prototype.scrollToBootom = function() {
    var height = this.ui.chatViewport.get(0).scrollHeight;
    this.ui.chatViewport.scrollTop(height);
};



NodeChat.prototype.chatTemplate =

'<div class="panel panel-default">'+
    '<div id="chat_container" class="panel-body chat">' +
    '<div id="chat_users_toggle" class="chat_users_toggle"></div>' +
    '<div id="chat_users" class="chat_users">' +
            '<div class="chat_users_viewport">'+
                '<div class="chat_users_cnt">'+
                    '<div id="chat_users_list" class="list-group"></div>'+
                '</div>'+
            '</div>'+
        '</div>'+

        '<div id="chat_vieport" class="chat_viewport">'+
            '<ul id="chat_messages" class="chat_viewport_cnt">'+
            '</ul>'+
        '</div>'+

    '</div>'+
    '<div class="panel-footer">'+
        '<form id="chat_form" class="form-horizontal" role="form">'+
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<div class="input-group">'+
                        '<input id="chat_input" type="text" class="form-control" placeholder="Message...">'+
                        '<span class="input-group-btn">'+
                            '<button id="chat_clear_input" class="btn btn-default" type="button">'+
                                '<span class="glyphicon glyphicon-remove"></span>'+
                            '</button>'+
                            '<button id="chat_send" class="btn btn-default" type="button">'+
                                '<span class="glyphicon glyphicon-pencil"></span> '+
                                'Send'+
                            '</button>'+
                        '</span>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</form>'+
    '</div>'+
'</div>';

