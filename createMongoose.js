var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/chat');

var schema = mongoose.Schema({
    name: String,
    test: Number
});
schema.methods.meow = function() {
    console.log( this.get('name') );
}

var Cat = mongoose.model('Cat', schema);

var kitty = new Cat({
    name: 'Zildjian',
    test: '4'
});

//console.log(kitty);

kitty.save(function (err, kitty, affected) {
    kitty.meow();
});