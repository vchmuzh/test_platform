var mongoose = require('libs/mongoose');
require('models/user');
mongoose.set('debug', true);

var async = require('async');
var log = require('libs/log')(module);

async.series({

    open:   open,
    drop:   dropDatabase,
    models: requireModels,
    create: createUsers

}, function(err, results) {
    console.log(err);
    console.log(results);

    mongoose.disconnect();
});

function open(callback) {
    console.log('state: ' + mongoose.connection.readyState);
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('models/user');

    async.each(Object.keys(mongoose.models), function(modelName, callback){
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

function createUsers(callback) {

    async.each([
        { username:'Vasya', password:'pass'},
        { username:'Vasya', password:'passpetya'},
        { username:'admin', password:'adminadmin'}
    ], function(item, callback) {
        var userItem = new mongoose.models.User(item);
        userItem.save(callback);
    }, callback);

    /*async.parallel([
        function(callback) {
            var vasya = new User({ username:'Vasya', password:'pass'});
            vasya.save(function(err) {
                callback(err, vasya);
            });
        },
        function(callback) {
            var petya = new User({ username:'Petya', password:'passpetya'});
            petya.save(function(err) {
                callback(err, petya);
            });
        },
        function(callback) {
            var admin = new User({ username:'admin', password:'adminadmin'});
            admin.save(function(err) {
                callback(err, admin);
            });
        }
    ], callback);*/
}

/*mongoose.connection.on('open', function() {
    var db = mongoose.connection.db;
    db.dropDatabase(function(err) {
        if(err) throw err;
        log.info('DB Dropped OK');

        async.parallel([
            function(callback) {
                var vasya = new User({ username:'Vasya', password:'pass'});
                vasya.save(function(err) {
                    callback(err, vasya);
                });
            },
            function(callback) {
                var petya = new User({ username:'Petya', password:'passpetya'});
                petya.save(function(err) {
                    callback(err, petya);
                });
            },
            function(callback) {
                var admin = new User({ username:'admin', password:'adminadmin'});
                admin.save(function(err) {
                    callback(err, admin);
                });
            }
        ], function(err, results) {
            console.log(arguments);
            mongoose.disconnect();
        });


    });
});*/

