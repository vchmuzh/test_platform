var User = require('./models/user').User;

var user = new User({
    username: 'User',
    password: 'secret2'
});

user.save(function(err, user, affected) {
    if(err) throw err;

    User.findOne({username: 'Tester'}, function(err, tester) {
        if(err) throw err;
        console.log(tester);
    })
});